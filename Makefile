.POSIX:

CC = cc
CFLAGS =
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

all: jornal
jornal: jornal.c
	$(CC) $(CFLAGS) -o jornal jornal.c

install: jornal
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f jornal $(DESTDIR)$(PREFIX)/bin

	mkdir -p $(MANPREFIX)/man1
	cp -f jornal.1 $(MANPREFIX)/man1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/jornal
	rm -f $(MANPREFIX)/man1/jornal.1

clean:
	rm -f jornal

debug:
	rm -f jornal
	gcc -g -std=c89 -Wall -Wextra -pedantic -Wmissing-prototypes -Wstrict-prototypes -Wold-style-definition -o jornal jornal.c

.PHONY: all install uninstall clean debug
