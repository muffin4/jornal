/* jornal - a dead simple command like diary */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

/* BEGIN CUSTOMIZATION BLOCK */

/* The path/name relative to $HOME where your .jornal file will be saved. Defaults to ".jornal" */
/* ex: set to ".config/diary" to set it to $HOME/.config/diary */
const char *jornal_file_path_from_home = ".jornal/jornal";

/* END CUSTOMIZATION BLOCK */

#define DATE_BUFFER_SIZE 12

int main(int argc, char *argv[]) {
	int i, j, ch;
	int newline = 1, write_args = 0;
	time_t epoch;
	struct tm now, recent;
	char *jornal_file_path;
	char *home_path;
	char date_buffer[DATE_BUFFER_SIZE];
	size_t date_count = 0;
	char *input_buf = NULL;
	FILE *jornal_file;

	home_path = getenv("HOME");

	if (home_path == NULL) {
		fprintf(stderr, "jornal: Could not get env variable 'HOME'");
		exit(1);
	}

	/* Create the .jornal file path by concatenating $HOME/ and the path to the jornal file (default to .jornal) */
	jornal_file_path = malloc(sizeof(char) * (strlen(home_path) + strlen(jornal_file_path_from_home) + 2));

	for (i = 0; home_path[i] != '\0'; ++i) {
		jornal_file_path[i] = home_path[i];
	}

	jornal_file_path[i] = '/';
	++i;

	for (j = 0; jornal_file_path_from_home[j] != '\0'; ++j) {
		jornal_file_path[i + j] = jornal_file_path_from_home[j];
		jornal_file_path[i + j + 1] = '\0';
	}

	jornal_file = fopen(jornal_file_path, "r");

	if (jornal_file == NULL) {
		/* TODO: Just make the file if it doesn't exist instead of crashing */
		fprintf(stderr, "jornal: Could not open file '%s'\n", jornal_file_path);
		exit(1);
	}

	if (argc > 1) {
		write_args = 1;
	} else {
		/* Have to load it into a buffer, otherwise the times will be wrong, since you can take a while to type out something when writing directly into stdin. I have to aquire the "now" time reading AFTER stdin as been read. */
		int ch;
		size_t ch_count = 0;
		size_t input_buf_size = 4096;

		input_buf = malloc(sizeof(*input_buf) * input_buf_size);

		for (;;) {
			ch = getchar();

			if (ch == EOF || ch == '\n') {
				break;
			}

			if (ch_count >= input_buf_size - 1) {
				input_buf_size *= 2;
				input_buf = realloc(input_buf, sizeof(*input_buf) * input_buf_size);
			}

			input_buf[ch_count] = ch;
			++ch_count;
		}

		input_buf[ch_count] = '\0';
	}

	epoch = time(NULL);
	now = *localtime(&epoch);

	/* Get most recent date from jornal_file */
	for (i = 0; i < DATE_BUFFER_SIZE; ++i) {
		date_buffer[i] = '\0';
	}

	while ((ch = getc(jornal_file)) != EOF) {
		if (newline) {
			newline = 0;

			if (ch == '~') {
				j = 0;
				for (;;) {
					ch = getc(jornal_file);
					if (ch == EOF || ch == '\n' || j >= DATE_BUFFER_SIZE - 1) {
						break;
					}

					date_buffer[j] = ch;
					++j;
				}

				date_buffer[j + 1] = '\0';
				++date_count;
			}
		}

		if (ch == '\n') {
			newline = 1;
		}
	}

	fclose(jornal_file);

	/* Process most recent date */
	if (date_count == 0) {
		recent.tm_year = now.tm_year;
		recent.tm_mon = now.tm_mon;
		recent.tm_mday = now.tm_mday;
	} else {
		int i, j;
		char field_buffer[6];
		int matched_delim = 0;
		int date_field = 0; /* 0 == YYYY; 1 == MM; 2 == DD */

		for (i = 0, j = 0; date_buffer[i] != '\0'; ++i) {
			if (date_buffer[i] == '-') {
				if (date_field == 0) {
					recent.tm_year = atoi(field_buffer) - 1900;
				}

				if (date_field == 1) {
					recent.tm_mon = atoi(field_buffer) - 1;
				}

				++date_field;
				matched_delim = 1;
				j = 0;
			}

			if (!matched_delim) {
				field_buffer[j] = date_buffer[i];
				field_buffer[j + 1] = '\0';
				++j;
			} else {
				matched_delim = 0;
			}
		}

		/* Dirty hack to get the last field */
		recent.tm_mday = atoi(field_buffer);
	}

	jornal_file = fopen(jornal_file_path, "a");

	/* special case where it's the first entry to the file */
	/* Notice the lack of leading \n in this one */
	if (date_count == 0) {
		fprintf(jornal_file, "~%d-%02d-%02d\n", now.tm_year + 1900, now.tm_mon + 1, now.tm_mday);
	}

	/* Check to see if we're still on the same date as today */
	if (recent.tm_mday != now.tm_mday || recent.tm_mon != now.tm_mon || recent.tm_year != now.tm_year) {
		fprintf(jornal_file, "\n~%d-%02d-%02d\n", now.tm_year + 1900, now.tm_mon + 1, now.tm_mday);
	}

	fprintf(jornal_file, "%02d:%02d:%02d ", now.tm_hour, now.tm_min, now.tm_sec);

	if (write_args) {
		for (i = 1; i < argc; ++i) {
			fprintf(jornal_file, "%s ", argv[i]);
		}
	} else {
		fprintf(jornal_file, "%s", input_buf);
	}

	fprintf(jornal_file, "\n");

	fclose(jornal_file);

	free(jornal_file_path);
	free(input_buf);

	return 0;
}
